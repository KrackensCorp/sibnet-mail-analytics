#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <map>

using namespace std;

int c[300001]; // Массив с номерами процессов 
int n = 0; // Номер строки или Колличество операций 
const int n_max = 100000000; // Максимальное количество процессов
int j, i = 0;


int main() {
    setlocale (LC_ALL, "RUS");
   /// Получение вчерашней даты и формирование пути вывода ///
    time_t now = time(0);
    tm *ltm = localtime(&now);
    int Year = 1900 + ltm->tm_year, Mon = 1 + ltm->tm_mon, day = ltm->tm_mday - 1;
    string YearString, MonString, dayString;
    YearString = to_string(Year);
    MonString = to_string(Mon);
    dayString = to_string(day);
    if (Mon < 10) {
        MonString = "0" + MonString;
        }
    if (day < 10) {
        dayString = "0" + dayString;
    }
    string dateString = YearString + "-" + MonString + "-" + dayString; 
    string pathFull = ( dateString + ".sorted.mail.log"); 
    //////////////////////////////////////////////////////////


    map <int,int> ;



    ///////////// Читаем файл, ищем максимум ////////////////
    fstream F, Check; // поток для чтения
    ofstream out; // поток для записи
    string pathNoSorted = ( dateString + ".log"); // Название обработанного свежего файла логов

    Check.open(pathNoSorted);
    int step; // Шагатель по значениям потока
    if (Check) {
        while (!Check.eof()) {
                Check >> step;  
                n++;
        }
        Check.close();
    } else { 
        cout << "Файл " << pathNoSorted << " не найден" << endl; 
    }
    n = (n / 2); // Значений в файле в два раза больше чем колличества операций
    int a[n], b[n]; // массивы с колличеством операций
    for (int nul = 0; nul <= n; nul++) { a[nul] = 0;}
    for (int nul = 0; nul <= n; nul++) { b[nul] = 0;}
    //////////////////////////////////////////////////////////

    int numStr = 1; // Четность столбца
    int proc = 0; // Номер элемента считываемого массива номеров процессов
    int num; // Номера процессов
    int strok[n]; //Массив с номерами строк

    F.open(pathNoSorted);
    if (F) {
        while (!F.eof()) {
            if (numStr%2 != 0) {
                F >> n; // Номер строки
                if(j < n) {
                    strok[j] = n;
                    j++;
                     cout << " " <<j<< endl;
                }
                numStr++;
            } else {
                F >> num; // Номер процесса
                if(i < n) {
                    a[i] = num;
                    i++;
                     cout << i << endl;
                }
                numStr++;
            }
        }
        F.close();

    } else { 
        cout << "Файл " << pathNoSorted << " не найден" << endl; 
        F.close();
    }

    /////////////// Сортировочка ////////////////////////////
    out.open(pathFull); // окрываем файл для записи
    if (out.is_open())
    {  

    for( j = 0; j < 300001; j++ ) 
    c[j] = 0;

    for( i = 0; i < n; i++ ) 
    c[a[i] + 1]++;
        
    for( j = 0; j < 300001; j++ ) 
    c[j] += c[j - 1];

    for( i = 0; i < n; i++ ) 
    b[c[a[i]]++] = a[i];

    for( i = 0; i < n; i++ ) 
    a[i] = b[i];

    for( c[a[i]] = 0; c[a[i]] < n; c[a[i]]++ )
    out << "" << b[c[a[i]]] << " " << endl ;

    }
}