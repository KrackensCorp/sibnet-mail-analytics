import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbProgressBarModule,
  NbSelectModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { AnalyticComponent } from './analytic.component';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbIconModule,
    NbTabsetModule,
    NbSelectModule,
    NbListModule,
    NbProgressBarModule,
  ],
  declarations: [
    AnalyticComponent,
  ],
  providers: [
  ],
})
export class Dashboard2Module { }
