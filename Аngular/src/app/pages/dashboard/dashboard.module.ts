import { NgModule } from '@angular/core';
import { NbCardModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import {GoogleChartsModule} from 'angular-google-charts';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    GoogleChartsModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
