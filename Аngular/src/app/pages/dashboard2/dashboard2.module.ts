import {NgModule} from '@angular/core';
import {NbCardModule} from '@nebular/theme';
import {ThemeModule} from '../../@theme/theme.module';
import {Dashboard2Component} from './dashboard2.component';
import {AnalyticComponent} from '../analytic/analytic.component';
import {GoogleChartsModule} from 'angular-google-charts';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    GoogleChartsModule,
  ],
  declarations: [
    Dashboard2Component,
    AnalyticComponent,
  ],
})
export class Dashboard2Module {
}
